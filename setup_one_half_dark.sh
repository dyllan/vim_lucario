#!/bin/bash

current_user() {
        if [ "$USER" == "root" ]; then
                install_vim
        else
                setup_vim
        fi
}

install_vim() {
        if yum remove -y vim-tiny && \
        yum install -y vim; then
                setup_vim
        else
                return 1
        fi

}

setup_vim() {
        if mkdir -p ~/.vim/colors/ && \
        touch ~/.vimrc && \
        cp -n onehalfdark.vim ~/.vim/colors/ && \
        echo 'syntax enable' > ~/.vimrc && \
        echo 'colorscheme onehalfdark' >> ~/.vimrc && \
        echo 'alias vi=vim' >> ~/.bashrc && \
        source ~/.bashrc; then
                return 0
        else
                return 1
        fi
}

current_user
